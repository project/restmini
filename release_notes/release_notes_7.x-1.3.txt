RESTmini 7.x-1.3


New features
------------
* Client: Added custom CA cert file option 'ssl_cacert_file'.
* Client: Check that option 'pass' is set and non-empty, when option 'user' is non-empty.
* Client: Fail if passed an unsupported option.
* Client: Whether to SSL verify peer by default is now overridable by variable 'restmini_client_sslverifydefnot' as well as class constant SSL_VERIFY_DEFAULT.
* Client now increases PHP execution timeout when request timeout (+10%) exceeds PHP timeout.

Bug fixes
---------
* Client: Fixed missing slash in common CA certs file path when that path contains 'private://'.

Security
--------
None

Install/update notes
--------------------
* Update database.
* Clear all cache.
