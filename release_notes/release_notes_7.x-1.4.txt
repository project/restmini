RESTmini 7.x-1.4


New features
------------
* New parameter validation framework.
* Service test endpoints may now declare their parameters and use the built-in validation.
* Client: new option 'status_vain_result_void', which flags that result() must return empty string if status code >=300.
* The service registry is now stored in cache instead of a conf variable.

Bug fixes
---------
* Client: Send Content-Length header for method POST, even when no POST vars, otherwise servers may respond with 413 Request Entity Too Large status.

Security
--------
None

Install/update notes
--------------------
* Update database.
* Clear all cache.
