RESTmini 7.x-1.1


Main issues
-----------
* New service test module, which supports testing sequences of operations.
* Lot's of stuff that wasn't fully implemented.
* Several bug fixes.


New features
------------
Service
* New requestHeaders() method.
* Save Drupal's 'q' GET var (the request path) in the currentResponder array, and remove 'q' from $_GET to make $_GET reflect net GET arguments sent by the client.
* PUT now populates $_POST if empty and the received Content-Type is application/x-www-form-urlencoded.
* Set empty Content-Type header for status 204 and 304, to override web server sending default (usually text/html) Content-Type header.
* ::isRouter() method tells if current request was routed by the restmini_service module.
* ::currentResponder() now accepts a property(name) argument, to return single responder property - including new derivative prpperties 'resource' and (qualified)'name'.
* The respond() and error() methods works even if current request wasn't routed by the restmini_service module.

Service Test
* Service test modules: restmini_service_test dependency implicit; turn all testing on/off by enabling/disabling restmini_service_test only.

Client
* Now supports common SSL CA certificates, and new certificate bundle may be downloaded by cron.
* Warn if the PHP cUrl extension isn't available, when enabling the client module.
* Reduced default timeouts to connect:5 and response:20 seconds.
* Renamed response_timeout to request_timeout.
* Check that protocol is http/https.
* Dedicated error codes for common cURL errors.
* Default connect and response timeouts are now configurable by Drupal variables.
* An empty response Content-Type header must be null (not false); and added response content length attribute; and do reset those content attributes.
* ::request() is now public and chainable (returns $this, not boolean).
* New method info().


Bug fixes
---------
Service
* Fixed that router didn't detect non-existent callback, because call_user_func[_array]() - contrary to documentation - doesn't return boolean false when the callback isn't callable (issue #2361459).

Client
* Must return result:null on parse error, result:false on request error, and info() must not return a response bucket.
* Shan't use CURLOPT_PUT, because that makes cUrl send 'Tranfer-Encoding: chunked' (which is no good unless uploading a file; and file uploading is not within the scope of the client module).
* Fixed wrong url-encoding of POST vars.
* Fixed that delete() actually sent GET.


Security
--------
None


Install/update notes
--------------------
* Optionally install the service test module.
* Clear all cache
