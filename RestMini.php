<?php
/**
 * @file
 * Drupal RestMini module.
 */


class RestMini {

  /**
   * @var integer
   */
  protected static $errorCodeOffset = 0;

  /**
   * @var array
   */
  protected static $errorCodes = array();

  /**
   * Get error code by name, or code list, or code range.
   *
   * @param string $name
   *   Non-empty: return code by name (defaults to 'unknown')
   *   Default: empty (~ return codes list).
   * @param boolean $range
   *   TRUE: return code range array(N-first, N-last).
   *   Default: FALSE (~ ignore argument).
   *
   * @return integer|array
   */
  public static function errorCode($name = '', $range = FALSE) {
    static $codes;

    if ($name) {
      return static::$errorCodeOffset
        + (array_key_exists($name, static::$errorCodes) ? static::$errorCodes[$name] : static::$errorCodes['unknown']);
    }

    if ($range) {
      return array(
        static::$errorCodeOffset,
        // Range of sub modules should only be 100, to allow for all sub modules within an overall range of 1000.
        static::$errorCodeOffset + 99
      );
    }

    if (!$codes) {
      $codes = static::$errorCodes; // Copy.
      if (($offset = static::$errorCodeOffset)) {
        foreach ($codes as &$code) {
          $code += $offset;
        }
        unset($code); // Iteration ref.
      }
    }

    return $codes;
  }


  /**
   * Must not be overridden as the property is used by methods (of RestMiniService) that are final.
   * Should be final, but PHP doesn't support final properties (only methods).
   *
   * @var array
   */
  protected static $supportedMethods = array('HEAD', 'GET', 'POST', 'PUT', 'DELETE');


  /**
   * Get status text of a status code.
   *
   * Does not take HTTP method into consideration - a 201 as response to PUT should rather be 'Updated' than 'Created'.
   *
   * @param integer $code
   *
   * @return string
   *   If unsupported status $code: 'Unknown Status'.
   */
  final public static function statusText($code) {
    // Stringify code to prevent coercion fault; a string matches any number in a PHP switch.
    switch ('' . $code) {
      case '200':
        return 'OK';
      case '201':
        return 'Created';
      case '204':
        return 'No Content';
      case '301':
        return 'Moved Permanently';
      case '302':
        return 'Found';
      case '304':
        return 'Not Modified';
      case '400':
        return 'Bad Request';
      case '401':
        return 'Unauthorized';
      case '403':
        return 'Forbidden';
      case '404':
        return 'Not Found';
      case '405':
        return 'Method Not Allowed';
      case '406':
        return 'Not Acceptable';
      case '410':
        return 'Gone';
      case '415':
        return 'Unsupported Media Type';
      case '500':
        return 'Internal Server Error';
      case '501':
        return 'Not Implemented';
      case '502':
        return 'Bad Gateway';
      case '503':
        return 'Service Unavailable';
      case '504':
        return 'Gateway Timeout';
    }
    return 'Unknown Status';
  }

  /**
   * Get info about appropriate use of HTTP response status codes to HTTP request methods.
   *
   * @param string $method
   *   Non-empty: list status codes usable by that HTTP method.
   *   Default: empty (~ list all status codes and the HTTP methods that may use them).
   *
   * @return array
   */
  final public static function statusCodes($method = '') {
    static $codes, $all_methods;

    if (!$codes) {
      $all_methods = self::$supportedMethods;
      $codes = array(
        200 => array('HEAD', 'GET', 'DELETE'), // OK.
        201 => array('POST', 'PUT'), // Created|Updated (with payload).
        204 => array('POST', 'PUT', 'DELETE'), // No Content.
        301 => $all_methods, // Moved Permanently.
        302 => $all_methods, // Found.
        304 => array('HEAD', 'GET'),
        400 => $all_methods, // Bad Request.
        401 => $all_methods, // Unauthorized.
        403 => $all_methods, // Forbidden.
        404 => $all_methods, // Not Found.
        405 => $all_methods, // Method Not Allowed.
        406 => $all_methods, // Not Acceptable.
        410 => $all_methods, // Gone.
        415 => array('POST', 'PUT'), // Unsupported Media Type.
        500 => $all_methods, // Internal Server Error.
        501 => $all_methods, // Not Implemented.
        502 => $all_methods, // Bad Gateway.
        503 => $all_methods, // Service Unavailable.
        504 => $all_methods, // Gateway Timeout.
      );
    }
    if (!$method) {
      return $codes;
    }
    $arr = array();
    if (in_array($method, $all_methods, TRUE)) {
      foreach ($codes as $status => $methods) {
        if (in_array($method, $methods)) {
          $arr[$status] = $status;
        }
      }
    }
    return $arr;
  }

  /**
   * Check if a successful status - 200, 201, 204, 304 - and that status and optionally content length matches HTTP method.
   *
   * Content length: consider that neither that neither '0' nor an empty array is an empty response.
   *
   * @param string $method
   *   HEAD|GET|POST|PUT|DELETE.
   * @param integer $status
   *   Integer: check against method.
   *   No argument: return array of successful status code of that method.
   * @param integer $contentLength
   *   Integer: check against method and status.
   *   No argument: don't consider content length.
   *
   * @return boolean|array|NULL
   *   Boolean: whether status (and optionally content length) matches method.
   *   Array: list of successful status code of that method.
   *   NULL: on error.
   */
  final public function statusSuccess($method, $status = 0, $contentLength = 0) {
    $nArgs = func_num_args();

    switch ($method) {
      case 'HEAD':
      case 'GET':
        $codes = array(200, 304);
        break;
      case 'POST':
      case 'PUT':
        $codes = array(201, 204); // @todo: W3C says 200 is fine too.
        break;
      case 'DELETE':
        $codes = array(200, 204);
        break;
      default:
        static::log(
          'restmini',
          'HTTP method not supported',
          NULL,
          $method,
          WATCHDOG_ERROR
        );
        return NULL;
    }

    if ($nArgs == 1) {
      return $codes;
    }

    if (!in_array($status, $codes)) {
      return FALSE;
    }
    if ($nArgs == 2) {
      return TRUE;
    }

    return ($method == 'HEAD' || $status == 204 || $status == 304) ? !$contentLength : !!$contentLength;
  }


  /**
   * Get a predefined validation pattern - a set of validation rules - or all predefined patterns.
   *
   * @throws Exception
   *   If failing to include this module's *.validation_patterns.inc file.
   *
   * @param string $name
   *   Default: empty (~ return all).
   *
   * @return array|NULL
   *   NULL: pattern by $name doesn't exist.
   */
  public static function validationPattern($name = '') {
    // An extending class which declares additional (or overriding) patterns could do like this in it's validationPattern() method:
    /*
    static $patterns;
    if (!$patterns) {
      include 'module_name.validation_patterns.inc';
      if (!isset($validation_patterns)) {
        throw new Exception('Failed to include validation patterns');
      }
      $patterns = array_merge(
        parent::validationPattern(),
        $validation_patterns
      );
    }

    if ($name) {
      return isset($patterns[$name]) ? $patterns[$name] : NULL;
    }
    return $patterns;
    */

    static $patterns;
    if (!$patterns) {
      include 'restmini.validation_patterns.inc';
      if (!isset($validation_patterns)) {
        throw new Exception('Failed to include validation patterns');
      }
      $patterns =& $validation_patterns;
    }

    if ($name) {
      return isset($patterns[$name]) ? $patterns[$name] : NULL;
    }
    return $patterns;
  }

  /**
   * @var array
   */
  protected static $validationFailures = array();

  /**
   * Get validation failures - key-message pairs - recorded by validate().
   *
   * @param boolean $keep
   *   Default: FALSE (~ flush upon retrieval).
   *
   * @return array
   */
  final public static function validationFailures($keep = FALSE) {
    if (!$keep) {
      $a = self::$validationFailures;
      self::$validationFailures = array();
      return $a;
    }
    return self::$validationFailures;
  }

  /**
   * Supported validation pattern rules.
   *
   * @see RestMini::validate()
   *
   * @var array
   */
  private static $validationPatternRulesSupported = array(
    'title',
    'type',
    'required',
    'optional',
    'default',
    'except_value',
    'except_type',
    'no_trim',
    'exact_length',
    'max_length',
    'min_length',
    'truncate',
    'min',
    'max',
    'check',
    'enum',
    'regex',
  );

  /**
   * Validates the existence or value of $key in $container, using $pattern as rule set.
   *
   *  Supported pattern rules (all optional):
   *  - (str) title; used for failure message
   *  - (str) type; default string, string|integer|float|array|object|boolean|NULL
   *  - (bool) required; default yes, the most restrictive of required and optional wins
   *  - (bool) optional; default not, the most restrictive of required and optional wins
   *  - (mixed) default; only applicable for unset value and for empty (type:)string
   *  - (mixed) except_value; if that and actual value are exactly the same, then it obsoletes all other rules but 'required' (cannot be array|object)
   *  - (string) except_type; if that and actual data type stringed are the same, then no type mismatch (only 'array'|'object'|'boolean'|'NULL' allowed)
   *  - (bool) no_trim; default not ~ do trim() a type:string value
   *  - (int) exact_length
   *  - (int) max_length
   *  - (int) min_length
   *  - (int) truncate; default don't, only applicable for type:string
   *  - (int) min;
   *  - (int) max;
   *  - (str) check; a predefined check, see restmini_validation_check()
   *  - (arr) enum; comparison performed type-safe if type:integer/float
   *  - (str) regex; a custom regular expression
   *
   * This method does not support 'pattern' as rule (ignores it), whereas RestMiniService::importValidateArguments() (indirectly) do.
   * _There_ it works as a way of copying all rules of a predefined pattern into the pattern defined for a path/get/post parameter.
   *
   *  Sequence of checks and conversions for non-number types:
   *  1) trim
   *  2) exact_length/max_length/min_length
   *  3) truncate
   *  4) check
   *  5) enum or regex (mutually exclusive)
   *
   *  A string value may get altered:
   *  - trimmed, unless pattern contains 'no_trim' flag
   *  - set, if empty and pattern contains a 'default' value
   *  - truncated, if pattern contains a 'truncate' value
   *  - converted to integer or float, if pattern 'type' is either
   *
   *  Sequence of checks and conversions for integer/float:
   *  1) exact_length/max_length/min_length
   *  2) min/max
   *  3) enum or regex (mutually exclusive)
   *
   *  An integer/float value may get altered:
   *  - cast float to integer (or vice versa) if pattern 'type' is the other
   *  - stringed, if pattern 'type' isn't integer/float
   *
   * Numbers can only be 10-scaled, no support for hex or other.
   * If both integer and float are acceptable types for a value, use float.
   *
   * Array/object type and value is only checked for existance and emptiness, and may use a 'default' if non-existent.
   * So the only rules applicable are: title, type, required and default.
   *
   * @throws Exception
   *   If $pattern is empty.
   *   If a pattern rule isn't supported.
   *   If the value of pattern 'enum' isn't array.
   *   If pattern 'regex' isn't a valid non-empty regular expression, delimited by slashes.
   *   Propagated, if a pattern attempts to check against a 'check' that doesn't exist.
   *
   * @param array $pattern
   *   Validation pattern; a list of rules and options.
   * @param array|object &$container
   *   By reference, because a string value may get trimmed, truncated or set to a default value.
   * @param string|integer $key
   *   Name or index of the value to validate.
   * @param boolean $containerIsObject
   *   Default: FALSE (~ $container is an array).
   *
   * @return boolean
   */
  public static function validate($pattern, &$container, $key, $containerIsObject = FALSE) {
    static $included_check = FALSE;

    if (!$pattern) {
      throw new Exception('Validation pattern is empty');
    }

    // A pattern containing an unsupported rule could easily result in insufficient validation.
    if (($unsupportedRules = array_diff(array_keys($pattern), self::$validationPatternRulesSupported))) {
      throw new Exception('Unsupported validation pattern rules[' . join(', ', $unsupportedRules) . ']');
    }

    $required = TRUE;
    $issetRequired = isset($pattern['required']);
    $issetOptional = isset($pattern['optional']);
    if (($issetRequired && $pattern['required']) || ($issetOptional && !$pattern['optional'])) {
      // Keep required.
    }
    elseif (($issetOptional && $pattern['optional']) || ($issetRequired && !$pattern['required'])) {
      $required = FALSE;
    }
    unset($issetRequired, $issetOptional, $pattern['required'], $pattern['optional']);

    // Default data type is (multibyte) string.
    if (!empty($pattern['type'])) {
      // Don't use 'double' for 'float'.
      if (($expectedType = $pattern['type']) == 'double') {
        $expectedType = 'float';
      }
    }
    else {
      $expectedType = 'string';
    }

    // Missing?
    // Has to be checked differently if the container is an object.
    if (!$containerIsObject) {
      if (!$container || !array_key_exists($key, $container)) {
        // Required?
        if ($required) {
          self::$validationFailures[$key] = $key . ' is missing.';
          return FALSE;
        }
        // A default?
        elseif (array_key_exists('default', $pattern)) {
          $container[$key] = $pattern['default'];
        }
        // Apparantly allowed to be non-existing.
        return TRUE;
      }
      $value = $container[$key];
    }
    else {
      if (!property_exists($container, $key)) {
        // Required?
        if ($required) {
          self::$validationFailures[$key] = $key . ' is missing.';
          return FALSE;
        }
        // A default?
        elseif (array_key_exists('default', $pattern)) {
          $container->{$key} = $pattern['default'];
        }
        // Apparantly allowed to be non-existing.
        return TRUE;
      }
      $value = $container->{$key};
    }

    // Whether value has been changed in any way during evaluation.
    $altered = FALSE;

    // Get and evaluate data type.
    // And leave - after some validation - if not scalar.
    $length = 0;
    $actualString = $actualNumber = $actualInteger = FALSE;
    switch (($actualType = gettype($value))) {

      case 'string':
        $actualString = TRUE;
        // Trim leading/trailing space chars, unless 'no_trim' flag.
        if (empty($pattern['no_trim'])) {
          $value = trim($value);
          $altered = TRUE;
        }
        // Empty string.
        if ($value === '') {
          // Required?
          if ($required) {
            self::$validationFailures[$key] = $key . ' is empty.';
            return FALSE;
          }
          // A default?
          elseif (array_key_exists('default', $pattern)) {
            if (!$containerIsObject) {
              $container[$key] = $pattern['default'];
            }
            else {
              $container->{$key} = $pattern['default'];
            }
            return TRUE;
          }
        }
        else {
          // Check character set.
          if (!drupal_validate_utf8($value)) {
            self::$validationFailures[$key] = $key . ' is not valid UTF-8.';
            return FALSE;
          }
          $length = drupal_strlen($value);
        }
        break;

      case 'integer':
        $actualNumber = $actualInteger = TRUE;
        $length = strlen('' . $value);
        break;

      case 'double':
      case 'float':
        $actualNumber = TRUE;
        $length = strlen('' . $value);
        break;

      case 'array':
        // That type must be expected, or allowed as exception.
        if ($expectedType != 'array'
          && (!isset($pattern['except_type']) || $pattern['except_type'] != 'array')
        ) {
          self::$validationFailures[$key] = $key . ' is array, should be ' . $expectedType . '.';
          return FALSE;
        }
        if (!$value) {
          // Required?
          if ($required) {
            self::$validationFailures[$key] = $key . ' is empty';
            return FALSE;
          }
          // A default?
          elseif (array_key_exists('default', $pattern)) {
            if (!$containerIsObject) {
              $container[$key] = $pattern['default'];
            }
            else {
              $container->{$key} = $pattern['default'];
            }
          }
        }
        return TRUE;

      case 'object':
        // That type must be expected, or allowed as exception.
        if ($expectedType != 'object'
          && (!isset($pattern['except_type']) || $pattern['except_type'] != 'object')
        ) {
          self::$validationFailures[$key] = $key . ' is object, should be ' . $expectedType . '.';
          return FALSE;
        }
        if (!get_object_vars($value)) {
          // Required?
          if ($required) {
            self::$validationFailures[$key] = $key . ' is empty';
            return FALSE;
          }
          // A default?
          elseif (array_key_exists('default', $pattern)) {
            if (!$containerIsObject) {
              $container[$key] = $pattern['default'];
            }
            else {
              $container->{$key} = $pattern['default'];
            }
          }
        }
        return TRUE;

      case 'boolean':
        if ($expectedType != 'boolean'
          && (!isset($pattern['except_type']) || $pattern['except_type'] != 'boolean')
          && (!isset($pattern['except_value']) || $pattern['except_value'] !== $value)
        ) {
          self::$validationFailures[$key] = $key . ' is boolean, should be ' . $expectedType . '.';
          return FALSE;
        }
        return TRUE;

      case 'NULL':
        if ($expectedType != 'NULL'
          && (!isset($pattern['except_type']) || $pattern['except_type'] != 'NULL')
          && (!array_key_exists('except_value', $pattern) || $pattern['except_value'] !== $value)
        ) {
          if (empty($pattern['allow_null'])) {
            self::$validationFailures[$key] = $key . ' is NULL, should be ' . $expectedType . '.';
            return FALSE;
          }
        }
        return TRUE;

      default:
        // Unsupported actual type; cannot match expected type.
        self::$validationFailures[$key] = $key . ' is ' . $actualType . ', should be ' . $expectedType . '.';
        return FALSE;
    }

    // From here on, the expected and actual data types are string, integer or float.
    // And we do not consider it a failure, if actual type - among these three types - differs from expected type.
    if (!$actualString && !$actualNumber) {
      self::$validationFailures[$key] = $key . ' is ' . $actualType . ', should be ' . $expectedType . '.';
      return FALSE;
    }

    // 'except_value' obsoletes all other rules (but required) if the rule value and the actual value are the same.
    if (isset($pattern['except_value']) && $value === $pattern['except_value']) {
      return TRUE;
    }

    // All types may have length requirements.
    // Algo: exact_length/max_length/min_length MUST be applied before truncate.
    if (!empty($pattern['exact_length'])) {
      if ($length > $pattern['exact_length']) {
        self::$validationFailures[$key] = $key . ' required length is ' . $pattern['exact_length'] . ', actual length ' . $length . '.';
        return FALSE;
      }
    }
    else {
      if (!empty($pattern['max_length'])) {
        if ($length > $pattern['max_length']) {
          self::$validationFailures[$key] = $key . ' max length is ' . $pattern['max_length'] . ', actual length ' . $length . '.';
          return FALSE;
        }
      }
      if (!empty($pattern['min_length'])) {
        if ($length < $pattern['min_length']) {
          self::$validationFailures[$key] = $key . ' min length is ' . $pattern['min_length'] . ', actual length ' . $length . '.';
          return FALSE;
        }
      }
    }

    // Numbers.
    // Will become true if number expected AND $value is confirmed as - or converted successfully to - a number.
    $isNumber = FALSE;

    if ($expectedType == 'integer') {
      // Actually a string.
      if (!$actualNumber) {
        if (!$value) {
          $value = 0;
        }
        // Only digits?
        elseif (!ctype_digit($value)) {
          // Not only digits, but it may be a float.
          // filter_var() actually converts, even when given a FILTER_VALIDATE_... flag.
          if (!($value = filter_var($value, FILTER_VALIDATE_FLOAT))
            || $value > 2147483647 || $value < -2147483648
            || !($value = filter_var($value, FILTER_VALIDATE_INT))
          ) {
            self::$validationFailures[$key] = $key . ' is not a 32-bit integer.';
            return FALSE;
          }
          // else... keep $value converted to integer.
        }
        // Only digits, but out of 32-bit range?
        elseif (($value = (float)$value) > 2147483647 || $value < -2147483648) {
          self::$validationFailures[$key] = $key . ' is not a 32-bit integer.';
          return FALSE;
        }
        else {
          $value = (int)$value;
        }
        // No matter which, it got altered.
        $altered = TRUE;
      }
      // Actually a float.
      elseif (!$actualInteger) {
        if ($value > 2147483647 || $value < -2147483648) {
          self::$validationFailures[$key] = $key . ' is not a 32-bit integer.';
          return FALSE;
        }
        else {
          $value = (int)$value;
          $altered = TRUE;
        }
      }
      $isNumber = TRUE;
    }
    elseif ($expectedType == 'float') {
      if (!$actualNumber) {
        if (!$value) {
          $value = 0;
        }
        // filter_var() actually converts, even when given a FILTER_VALIDATE_... flag.
        elseif (!($value = filter_var($value, FILTER_VALIDATE_FLOAT))) {
          self::$validationFailures[$key] = $key . ' is not a float.';
          return FALSE;
        }
        // else... keep $value converted to float.
        $altered = TRUE;
      }
      elseif ($actualInteger) {
        $value = (float)$value;
        $altered = TRUE;
      }
      $isNumber = TRUE;
    }
    // Stringy types.
    else {
      // Cast actual number to string.
      if ($actualNumber) {
        $value = '' . $value;
        $altered = TRUE;
      }

      // Truncate?
      // Algo: truncate MUST be applied before enum/regex.
      if (!empty($pattern['truncate'])) {
        $value = drupal_substr($value, 0, $pattern['truncate']);
        $altered = TRUE;
      }

      // A predefined 'check'.
      if (!empty($pattern['check'])) {
        if (!$included_check) {
          $included_check = TRUE;
          module_load_include('inc', 'restmini', 'restmini_validation_check');
        }

        if (!restmini_validation_check($pattern['check'], $value)) {
          self::$validationFailures[$key] = $key . ' '
            . (!empty($pattern['title']) ? ('is not ' . $pattern['title']) : 'does not match valid pattern') . '.';
          return FALSE;
        }
      }
    }

    // Expected AND confirmed (or converted to) number.
    if ($isNumber) {
      // Min/max.
      if (!empty($pattern['min']) && $value < $pattern['min']) {
        self::$validationFailures[$key] = $key . ' minimum is ' . $pattern['min'] . ', saw ' . $value . '.';
        return FALSE;
      }
      if (!empty($pattern['max']) && $value > $pattern['max']) {
        self::$validationFailures[$key] = $key . ' maximum is ' . $pattern['max'] . ', saw ' . $value . '.';
        return FALSE;
      }
    }

    // enum.
    if (!empty($pattern['enum'])) {
      $enum = $pattern['enum'];
      if (!is_array($enum)) {
        throw new Exception('Validation pattern enum must be array, is ' . gettype($enum));
      }

      // Compare type-safe if rule 'type' is integer|float; that will confuse some, but there's no way other way around it.
      if (!in_array($value, $enum, $isNumber)) {
        self::$validationFailures[$key] = $key . ' is not '
          . (!empty($pattern['title']) ? $pattern['title'] : 'within range of allowed values') . '.';
        return FALSE;
      }
    }
    // enum cannot be combined with regex.
    elseif (!empty($pattern['regex'])) {
      if (!($match = preg_match($pattern['regex'], '' . $value))) {
        // If preg_match() returns FALSE (or NULL?) it's an error, not a validation failure.
        if ($match !== 0) {
          $regex = $pattern['regex'];
          if (strlen($regex) < 3) {
            $em = 'is too short to contain leading and trailing slash delimiter, and at least a single character class';
          }
          elseif ($regex{0} !== '/') {
            $em = 'misses leading slash delimiter';
          }
          else {
            $em = 'is not a valid regular expression';
          }
          throw new Exception('Validation regex ' . $regex . ' ' . $em);
        }

        // Value failed regex validation.
        self::$validationFailures[$key] = $key . ' '
          . (!empty($pattern['title']) ? ('is not ' . $pattern['title']) : 'does not match valid pattern') . '.';
        return FALSE;
      }
    }

    // Passed validation.
    if ($altered) {
      if (!$containerIsObject) {
        $container[$key] = $value;
      }
      else {
        $container->{$key} = $value;
      }
    }
    return TRUE;
  }

  /**
   * Logs to watchdog.
   *
   * Traces Exception, if $exception and the Inspect module exists.
   *
   * @param string $type
   * @param string $message
   * @param Exception|NULL $exception
   * @param mixed $variable
   * @param integer $severity
   */
  protected static function log($type, $message, $exception = NULL, $variable = NULL, $severity = WATCHDOG_ERROR) {
    if (module_exists('inspect')) {
      if ($exception) {
        inspect_trace($exception, array(
          'category' => $type,
          'message' => $message,
          'severity' => $severity,
          'wrappers' => 1,
        ));
      }
      else {
        inspect($variable, array(
          'category' => $type,
          'message' => $message,
          'severity' => $severity,
          'wrappers' => 1,
        ));
      }
    }
    else {
      watchdog(
        $type,
        (!$message && $exception) ? $exception->getMessage() : $message,
        array(),
        $severity
      );
    }
  }

}
