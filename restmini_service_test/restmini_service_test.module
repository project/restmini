<?php
/**
 * @file
 * RESTmini Service Test (restmini_service_test)
 */


/**
 * Implements hook_menu().
 *
 * @return array
 */
function restmini_service_test_menu() {
  return array(
    'admin/config/restmini/service_test' => array(
      'title' => 'Test services',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('restmini_service_test_execute_form'),
      'access arguments' => array('administer site configuration'),
      'file' => 'admin/restmini_service_test.admin.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => -15,
    )
  );
}

/**
 * Execute test(s) against service endpoints.
 *
 *  Options:
 *  - (str) output_target: print|watchdog|file (default: print)
 *  - (bool|int) response_info: tells the REST client to wrap response payload in client and service info wrappers (default: not)
 *  - (bool|int) force_complete: a RestMiniClient option (default: not defined)
 *  - (bool|int) ssl_verify: a RestMiniClient option (default: not defined)
 *  - (str) user: a RestMiniClient option, ignored if no pass (default: none)
 *  - (str) pass: a RestMiniClient option, ignored if no user (default: none)
 *
 * @see hook_restmini_service_test()
 * @see restmini_service_example_test_restmini_service_test()
 *
 * @param string $server
 *   Protocol + domain (~ http://ser.ver).
 *   Prepends http:// if no protocol (only http and https supported).
 *   Trailing slash will be removed.
 * @param string $tests
 *   Comma-separated string of test names.
 *   Default: all.
 * @param array $options
 *   Default: empty.
 */
function restmini_service_test($server, $tests = 'all', $options = array()) {
  module_load_include('inc', 'restmini_service_test', 'inc/RestMiniServiceTestVar');
  module_load_include('inc', 'restmini_service_test', 'inc/RestMiniServiceTest');
  RestMiniServiceTest::executeSelected($server, $tests, $options);
}
